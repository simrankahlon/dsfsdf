from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import json
import logging
from typing import Text, Dict, Any, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import SlotSet, UserUtteranceReverted, \
    ConversationPaused, FollowupAction, Form
from rasa_sdk.events import SlotSet, Form

logger = logging.getLogger(__name__)

class ActionDefaultAskAffirmation(Action):
    """Asks for an affirmation of the intent if NLU threshold is not met."""

    def name(self) -> Text:
        return "action_default_ask_affirmation"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]
            ) -> List['Event']:

        try:
            ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
            fallback_message = '[{ "message": { "template": { "elements": { "title": "Hello Blue Mountain How can I help you?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>Hello How can I help you?</speak>"}]'
            dispatcher.utter_message(fallback_message)
        except:
            print("Could'nt enter the missed utterance")
        return []